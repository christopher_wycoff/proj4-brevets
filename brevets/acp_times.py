"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.

    """
    if control_dist_km > brevet_dist_km:
      control_dist_km = brevet_dist_km


    #print("final_adustment" , final_adustment)
    # establish some parts 
    lower_part = 200/34
    lower_middle_part = 200/32
    upper_middle_part = 200/30
    upper_part = 400/28

    temp_control_distance = control_dist_km

    if control_dist_km >= 0 and control_dist_km <= 200:
      the_shift_from_start = control_dist_km/34

    elif control_dist_km > 200 and control_dist_km <= 400:

      temp_control_distance -= 200 

      the_shift_from_start = (temp_control_distance/32 + lower_part)

    elif control_dist_km > 400 and control_dist_km <= 600:

      temp_control_distance -= 400 

      the_shift_from_start = (temp_control_distance/30 + lower_part + lower_middle_part)

    elif control_dist_km > 600 and control_dist_km <= 1000:

      temp_control_distance -= 600 

      the_shift_from_start = (temp_control_distance/28 + lower_part + lower_middle_part+ upper_middle_part)

    else:
      the_shift_from_start = (temp_control_distance/26 + upper_part + lower_part + lower_middle_part+ upper_middle_part)

    
    ### ^^^ the above else is never entered

    
    brevet_start_time = brevet_start_time.shift(hours=the_shift_from_start)


    #print("now", arrow.now().isoformat())
    #print("then", brevet_start_time.isoformat())

    #print("hiiii!",arrow.now().isoformat())
    return brevet_start_time.isoformat()[:-6]+"-8:00"


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    if control_dist_km > brevet_dist_km:
      control_dist_km = brevet_dist_km


     # establish some parts 
    lower_part = 600/15
    upper_part = 400/11.428
    

    temp_control_distance = control_dist_km


    if control_dist_km >= 0 and control_dist_km <= 600:
      the_shift_from_start = control_dist_km/15

    elif control_dist_km > 600 and control_dist_km <= 1000:
      temp_control_distance -= 600
      the_shift_from_start = temp_control_distance/11.428 + lower_part

    else:
      temp_control_distance -= 1000
      the_shift_from_start = temp_control_distance/13.333 + lower_part + upper_part

      ### ^^^ the above else is never entered


    brevet_start_time = brevet_start_time.shift(hours=the_shift_from_start)



    return brevet_start_time.isoformat()[:-6]+"-8:00"












