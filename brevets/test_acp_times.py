"""
Nose tests for acp_time.py
"""


#import nose    # Testing framework
import arrow
from acp_times import open_time, close_time
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)

#### Table driven logic ####
controle_table = [100,200,300,400,500,600,700,800,900,1000]
brevet_table = [200,300,400,600,1000]

arrow_object =  arrow.now()

def test_regular_brevets():
    """
    open_times testing regular contoles
    with brevet distance 1000
    """
    
    controle_table = [200,300,400,600,1000]
    times = [5.89,9,12.1333333,18.784,33.084]
    the_time = arrow.get('2019-01-01T00:00:00.00-08:00')
    print("Testing Basic Controle Times")

    for i in range(len(times)):
        the_time = arrow.get('2019-01-01T00:00:00.00-08:00')
        brevet_start_time = the_time.shift(hours=times[i])
        brevet_start_time = brevet_start_time.isoformat()

        print(brevet_start_time,open_time(controle_table[i],1000,the_time))

        assert (open_time(controle_table[i],1000,the_time))[:15] == (brevet_start_time[:-6]+"-8:00")[:15]

    


def test_irregular_brevets():
    """
    Testing some non %100 = 0 distances
    """
    controle_table = [247,388,405,550,889]
    times = [7.35,11.75,12.283,17.12666,29.11666]

    the_time = arrow.get('2019-01-01T00:00:00.00-08:00')
    print("Testing Basic Controle Times")

    for i in range(len(times)):
        the_time = arrow.get('2019-01-01T00:00:00.00-08:00')
        brevet_start_time = the_time.shift(hours=times[i])
        brevet_start_time = brevet_start_time.isoformat()

        print(brevet_start_time,open_time(controle_table[i],1000,the_time))

        assert (open_time(controle_table[i],1000,the_time))[:15] == (brevet_start_time[:-6]+"-8:00")[:15]

def test_over_brevet_200():
    """
    Testing 250 in a 200 brevet open and close times
    """
    
    the_time = arrow.get('2019-01-01T00:00:00.00-08:00')

    brevet_open_time = the_time.shift(hours=5.87)
    brevet_close_time = the_time.shift(hours=13.433)
    brevet_open_time = brevet_open_time.isoformat()
    brevet_close_time = brevet_close_time.isoformat()

    print(brevet_open_time, (open_time(250,200,the_time)))
    print( brevet_close_time,(close_time(250,200,the_time)))
   

    assert (open_time(250,200,the_time))[:15] == (brevet_open_time[:-6]+"-8:00")[:15]

    assert (close_time(250,200,the_time))[:15] == (brevet_close_time[:-6]+"-8:00")[:15]


def test_close_times_brevets():
    """
    close_times testing regular contoles
    with brevet distance 1000
    """
    
    controle_table = [200,300,400,600,1000]
    times = [13.34,20,26.67,40,75]
    the_time = arrow.get('2019-01-01T00:00:00.00-08:00')
    print("Testing Basic Controle Times")

    for i in range(len(times)):
        the_time = arrow.get('2019-01-01T00:00:00.00-08:00')
        brevet_start_time = the_time.shift(hours=times[i])
        brevet_start_time = brevet_start_time.isoformat()

        print(brevet_start_time, close_time(controle_table[i],1000,the_time))

        assert (close_time(controle_table[i],1000,the_time))[:15] == (brevet_start_time[:-6]+"-8:00")[:15]

def test_test_floats():
    """
    Testing 100,
    """

    controle_table = [350.5,670.2,805.9]
    open_times = [10.5833,21.3,26.15]
    close_times = [23.37,46.133333,58.0166]
    the_time = arrow.get('2019-01-01T00:00:00.00-08:00')
    print("Testing Basic Controle Times")

    for i in range(len(open_times)):
        the_time = arrow.get('2019-01-01T00:00:00.00-08:00')
        brevet_start_time = the_time.shift(hours=open_times[i])
        brevet_start_time = brevet_start_time.isoformat()

        print(brevet_start_time, open_time(controle_table[i],1000,the_time))

        assert (open_time(controle_table[i],1000,the_time))[:15] == (brevet_start_time[:-6]+"-8:00")[:15]


    for i in range(len(close_times)):
        the_time = arrow.get('2019-01-01T00:00:00.00-08:00')
        brevet_start_time = the_time.shift(hours=close_times[i])
        brevet_start_time = brevet_start_time.isoformat()

        print(brevet_start_time, open_time(controle_table[i],1000,the_time))

        assert (close_time(controle_table[i],1000,the_time))[:15] == (brevet_start_time[:-6]+"-8:00")[:15]














